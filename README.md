<div align="center">

<img src="./assets/RnOS_Polygon_Final.png" width="150" height="150">

# System Configuration for RnOS

</div>

<br>

- The `data` directory contains all the configuration files for `qtile`, `rofi`, `fish`, user `systemd` units etc. which need to be placed in
the *non-root* user's `home` directory.
- This package is installed in `/etc/skel` directory, during the ISO build procedure.
- Once, the package is installed, all the files are copied into the liveuser's `home` directory from `/etc/skel`.
- These config files are then also carried over to the actual user's home directory during installation with `calamares`.
