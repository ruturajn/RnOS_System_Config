#!/bin/bash

## Using feh to set the background on startup
# feh --bg-scale /usr/share/Wallpapres/5120x2880.jpg

## Using nitrogen to set wallpaper on startup
nitrogen --restore

## Starting compton compositor on startup for transparency
# If transparency is not working, try with vsync
picom --no-vsync &

# Start pulseaudio
pulseaudio --start

# Start polkit
lxpolkit &

nitrogen --set-scaled /usr/share/Wallpapers/Mountains.jpg --save
~/.config/qtile/Scripts/get_ip.sh &
